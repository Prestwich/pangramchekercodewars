package com.company;

import java.util.HashSet;

public class PangramChecker {
	String alphabet = "abcdefghijklmnopqrstuvwxyz";

	public boolean check(String sentence){
		HashSet<Character> containedLetters = getContainedLettersFromSentence(sentence);

		// This is 2 int's and not Integer objects so  we need == operator.
		if(alphabet.length() == containedLetters.size()){
			return true;
		}
		return false;
	}

	private boolean inAlphabet(Character letterCheck){
		return alphabet.contains(String.valueOf(letterCheck));
	}

	private HashSet<Character> getContainedLettersFromSentence(String sentence) {
		HashSet<Character> containedLetters = new HashSet<>();
		for(Character letterCheck : sentence.toLowerCase().toCharArray()){
			if(inAlphabet(letterCheck)){
				//add to list of letters sentence does contain.
				containedLetters.add(letterCheck); // If duplicate letter this is a no op.
			}
		}
		return containedLetters;
	}
}
