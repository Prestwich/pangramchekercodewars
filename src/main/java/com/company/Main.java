package com.company;

import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Main {

    public static void main(String[] args) {
        test1();
        test2();
        test3();
    }
    public static void test1() {
        String pangram1 = "The quick brown fox jumps over the lazy dog.";
        PangramChecker pc = new PangramChecker();
        assertTrue(pc.check(pangram1));
    }

    @Test
    public static void test2() {
        String pangram2 = "You shall not pass!";
        PangramChecker pc = new PangramChecker();
        assertFalse(pc.check(pangram2));
    }

    @Test
    public static void test3() {
        String pangram2 = "The quickddd brown fox jumps over the lazy doggg!";
        PangramChecker pc = new PangramChecker();
        assertTrue(pc.check(pangram2));
    }
}
